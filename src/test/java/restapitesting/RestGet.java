package restapitesting;
import org.testng.annotations.Test;
import static com.jayway.restassured.RestAssured.given;

import java.io.IOException;

import org.testng.annotations.Test;

import com.jayway.restassured.RestAssured;
import com.jayway.restassured.response.Headers;
import com.jayway.restassured.response.Response;

public class RestGet {

	@Test(enabled=true)
	public void getCheck() {
		RestAssured.baseURI = "https://reqres.in/";
		//RestAssured.useRelaxedHTTPSValidation("TLSv1.2");
		Response response = given().when().get("api/users?page=2");
		System.out.println(response.asString());

	}
	
	
	
	
	
	
	
	@Test(enabled=false)
	public void getConsole() throws IOException {
		
		Headers head=Utility.getHTTPHeaders("./src/test/resources/header.properties");
		
		RestAssured.baseURI = "https://10.10.69.193:8086/";
		Response response = given().headers(head).when().get("application/get");
		System.out.println(response.asString());

	}
	
	
	
}
