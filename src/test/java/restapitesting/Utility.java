package restapitesting;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.net.Inet4Address;
import java.util.ArrayList;
import java.util.Enumeration;
import java.util.List;
import java.util.Properties;

import com.jayway.restassured.response.Header;
import com.jayway.restassured.response.Headers;

public class Utility {

	public static Headers getHTTPHeaders(String path) throws IOException {
		List<Header> list = new ArrayList<Header>();
		File file = new File(path);
		FileInputStream fileInput = new FileInputStream(file);
		Properties properties = new Properties();
		properties.load(fileInput);
		fileInput.close();
		Enumeration enuKeys = properties.keys();
		while (enuKeys.hasMoreElements()) {
			String key = (String) enuKeys.nextElement();
			String value = properties.getProperty(key);
			/*
			 * if (key.contains("IP")) // Replaceing with current IP instead of
			 * // spaces value = Inet4Address.getLocalHost().getHostAddress();
			 * else if (key.contains("USER")) // Replaceing with current
			 * Username // instead of spaces value =
			 * System.getProperty("user.name"); if
			 * (key.equalsIgnoreCase("HostID") != true ||
			 * key.equalsIgnoreCase("Port") != true)
			 */
			Header h = new Header(key, value);
			list.add(h);

		}
		Headers header = new Headers(list); // HTTP HEADER For All Requests
		return header;
	}

}
